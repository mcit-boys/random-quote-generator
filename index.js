const btnEl = document.getElementById("btn");
const quoteEl = document.getElementById("quote");
const authorEl = document.getElementById("author");
const apiURL = "https://api.quotable.io/random";

async function getQuote(){
    try {
        btnEl.innerText = "Loading...";
        btnEl.Disabled = true;
        quoteEl.innerText = "Updating...";
        authorEl.innerText = "Updating...";
        const response = await fetch(apiURL);
        const data = await response.json();
        const dataContent = data.content;
        const dataAuthor = data.author;
        quoteEl.innerText = dataContent;
        authorEl.innerText = "~ " +  dataAuthor;
        btnEl.innerText = "Get a Quote";
        btnEl.Disabled = false;
    } catch (error) {
        quoteEl.innerText = "An error happened! Try again later.";
        authorEl.innerText = "";
        btnEl.innerText = "Get a Quote";
        btnEl.Disabled = false;
    }
}
getQuote();
btnEl.addEventListener("click", getQuote);